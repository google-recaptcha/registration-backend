package com.recaptcha.rest;

import com.recaptcha.domain.Login;
import com.recaptcha.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class LoginRestController {
    @Autowired
    LoginService loginService;

    @PostMapping("validate-email")
    public Login validateEmail (@RequestBody Login login) {
        return loginService.validateEmail(login);
    }
    @PostMapping("validate-email-invisible-recaptcha")
    public Login validateEmailInvisibleRecaptcha (@RequestBody Login login) {
        return loginService.validateEmailInvisibleRecaptcha(login);
    }
}
