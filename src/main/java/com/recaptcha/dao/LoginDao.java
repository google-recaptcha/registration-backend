package com.recaptcha.dao;

import com.recaptcha.domain.Login;

public interface LoginDao {
    Login validateEmail(Login login);
}
