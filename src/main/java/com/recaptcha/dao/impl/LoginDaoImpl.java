package com.recaptcha.dao.impl;

import com.recaptcha.dao.LoginDao;
import com.recaptcha.domain.Login;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Repository
public class LoginDaoImpl implements LoginDao {
    List<Login> logins = new ArrayList<>();
    @Override
    public Login validateEmail(Login login) {
        generateLoginValues();
        List<Login> found = logins
                .stream()
                .filter(p -> p.getEmail().equals(login.getEmail()))
                .collect(Collectors.toList());
        return found.size() == 1 ? found.get(0): null;
    }
    private void generateLoginValues() {
        logins = new ArrayList<>();
        logins.add(new Login("gonzalomamani.dev@gmail.com", "123"));
        logins.add(new Login("gonzalo235711@gmail.com", "123"));
        logins.add(new Login("gmg.software.developer@gmail.com", "123"));
        logins.add(new Login("mariadelgado@gmail.com", "123"));
    }
}
