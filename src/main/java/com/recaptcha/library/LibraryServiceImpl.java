package com.recaptcha.library;

import com.recaptcha.domain.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Transactional
@Repository
public class LibraryServiceImpl implements LibraryService {
    @Autowired
    private Environment env;

    @Override
    public boolean validateReCaptcha(String token) {
        String url = env.getProperty("url-validate");
        String secret = env.getProperty("token-secret");
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url).queryParam("secret", secret).queryParam("response", token);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Response> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET,	entity, Response.class);
        Response rs = response.getBody();
        if (rs.isSuccess()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean validateReCaptchaInvisible(String token) {
        String url = env.getProperty("url-validate");
        String secret = env.getProperty("token-secret-invisible");
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url).queryParam("secret", secret).queryParam("response", token);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Response> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET,	entity, Response.class);
        Response rs = response.getBody();
        if (rs.isSuccess()) {
            return true;
        }
        return false;
    }
}
