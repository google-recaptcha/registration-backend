package com.recaptcha.library;

public interface LibraryService {
    public boolean validateReCaptcha(String token);
    public boolean validateReCaptchaInvisible(String token);
}
