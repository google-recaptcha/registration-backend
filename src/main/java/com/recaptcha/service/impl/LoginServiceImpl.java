package com.recaptcha.service.impl;

import com.recaptcha.dao.LoginDao;
import com.recaptcha.domain.Login;
import com.recaptcha.library.LibraryService;
import com.recaptcha.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class LoginServiceImpl implements LoginService {
    @Autowired
    LibraryService libraryService;

    @Autowired
    LoginDao loginDao;
    @Override
    public Login validateEmail(Login login) {
        Login response = loginDao.validateEmail(login);
        if (libraryService.validateReCaptcha(login.getToken())) {
            if (response != null) {
                response.setStatus("success");
            } else {
                response = new Login();
                response.setStatus("Email not found");
            }
        } else {
            response.setStatus("Invalid recaptcha token");
        }
        return response;
    }

    @Override
    public Login validateEmailInvisibleRecaptcha(Login login) {
        Login response = loginDao.validateEmail(login);
        if (libraryService.validateReCaptchaInvisible(login.getToken())) {
            if (response != null) {
                response.setStatus("success");
            } else {
                response = new Login();
                response.setStatus("Email not found");
            }
        } else {
            response.setStatus("Invalid recaptcha token");
        }
        return response;
    }
}
