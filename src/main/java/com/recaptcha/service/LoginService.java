package com.recaptcha.service;

import com.recaptcha.domain.Login;

public interface LoginService {
    Login validateEmail(Login login);
    Login validateEmailInvisibleRecaptcha(Login login);
}
